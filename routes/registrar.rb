require 'yaml'

module Sinatra
  module RegisterApp
    module Routing
      module Registrar
        def self.registered(app)
          app.get '/' do
            erb :index
          end

          app.post '/create' do
            request.body.rewind  # in case someone already read it
            # data = request.body.read
            puts request.params
            @name = request.params['name']
            @email = request.params['email']
            @namespace = request.params['namespace']

            # create resources
            init
            namespace = create_ns( @namespace )
            role = create_role( @namespace )
            role_binding = create_rolebinding( @namespace, 'default', role.metadata.name )

            @config = create_config( @namespace, 'default' )

            # give status of create
            erb :create
          end
        end
      end
    end
  end
end
