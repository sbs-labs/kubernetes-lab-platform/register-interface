require 'sinatra'
require 'rack/protection'
require File.dirname(__FILE__) + '/app'

configure do
  use Rack::Session::Cookie, :secret => "TOPSECRET"
  use Rack::Protection
end

run RegisterApp

