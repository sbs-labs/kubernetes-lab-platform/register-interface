ENV['APP_ENV'] = 'TEST'

require_relative '../app'
require 'rspec'
require 'rack/test'

describe 'Registrar app' do
  include Rack::Test::Methods

  def app
    RegisterApp
  end

  it "loads registration page" do
    get '/'
    expect(last_response).to be_ok
  end
end
