ENV['APP_ENV'] = 'TEST'

require_relative '../app'
require_relative '../helpers/kubernetes'
require 'rspec'
require 'rack/test'
require 'date'

describe 'Kubernetes Helper' do
  include Rack::Test::Methods
  include Sinatra::RegisterApp::Kubernetes

  bearer_token = 'bearer_token'

  it "can create a name space" do
    date = Date.today.to_s
    namespace_name = "test-ns_#{date}"
    namespace_json = { metadata: { name: namespace_name } }

    allow(Http).to receive(:auth) { Http }
    allow(Http).to receive(:post) { JSON.generate( namespace_json ) }

    namespace = create_ns( namespace_name )
    expect(namespace.metadata.name).to eq( namespace_name )
  end

  it "can create a role" do
    date = Date.today.to_s
    role_name = "test-role_#{date}"
    role_json = { metadata: { name: role_name } }

    allow(Http).to receive(:auth) { Http }
    allow(Http).to receive(:post) { JSON.generate( role_json ) }

    role = create_role( role_name )
    expect( role.metadata.name ).to eq( role_name )
  end

  it 'can create a rolebinding' do
    date = Date.today.to_s
    rolebinding_name = "test-rolebinding_#{date}"
    serviceaccount_name = "test-serviceaccount_#{date}"
    namespace_name = "test-namespace_#{date}"
    rolebinding_json = { metadata: { name: rolebinding_name } }

    allow(Http).to receive(:auth) { Http }
    allow(Http).to receive(:post) { JSON.generate( rolebinding_json ) }

    rolebinding = create_rolebinding( namespace_name, serviceaccount_name, rolebinding_name )
    expect( rolebinding.metadata.name ).to eq( rolebinding_name )
  end

  it 'can get a serviceaccount secret name' do
    date = Date.today.to_s
    namespace_name = "test-namespace_#{date}"
    serviceaccount_name = "test-serviceaccount_#{date}"
    serviceaccount_json = { secrets: [ { name: "secret-#{serviceaccount_name}" } ] }

    allow(Http).to receive(:auth) { Http }
    allow(Http).to receive(:get) { JSON.generate( serviceaccount_json ) }

    serviceaccount_secret_name = get_serviceaccount_secret_name( namespace_name, serviceaccount_name )
    expect( serviceaccount_secret_name ).to eq( "secret-#{serviceaccount_name}" )
  end

  it 'can get a secret' do
    date = Date.today.to_s
    namespace_name = "test-namespace_#{date}"
    serviceaccount_name = "test-serviceaccount_#{date}"
    secrets_json = { data: "stuff" }

    allow(Http).to receive(:auth) { Http }
    allow(Http).to receive(:get) { JSON.generate( secrets_json ) }

    secret_data = get_secret( namespace_name, serviceaccount_name )
    expect( secret_data ).to eq( "stuff" )
  end

  it 'can generate configurations' do
    date = Date.today.to_s
    namespace_name = "test-namespace_#{date}"
    serviceaccount_name = "test-serviceaccount_#{date}"
    secrets_json = OpenStruct.new('ca.crt': "CA.CRT", token: "ENC_TOKEN" )

    allow(self).to receive(:get_serviceaccount_secret_name) { "secret_name" }
    allow(self).to receive(:get_secret) { secrets_json }
    allow(Base64).to receive(:decode64) { "TOKEN" }

    config = create_config( namespace_name, serviceaccount_name )
    expect( config[:'current-context'] ).to eq( "lab-environment" )
  end

end
