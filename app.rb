require 'sinatra/base'

require_relative 'helpers/kubernetes'
require_relative 'routes/registrar'


class RegisterApp < Sinatra::Base

  set :bind, '0.0.0.0'

  enable :sessions

  helpers Sinatra::RegisterApp::Kubernetes

  register Sinatra::RegisterApp::Routing::Registrar

end
