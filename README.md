# Lab Registration Interface
web app to register and create a namespace for lab applications

## Running the application

```bash
# Start docker container
# docker run -it -v $(pwd):/usr/src/app -w /usr/src/app -u $(id -u):$(id -g) -p 4567:4567 ruby bash
./bin/container

# need to install dependencies
bundle install

# inside the container to start the web server
# bundle exec rackup config.ru -p 4567 -o 0.0.0.0
bundle exec foreman start web
```
