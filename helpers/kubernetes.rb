require 'http'
require 'base64'

module Sinatra
  module RegisterApp
    module Kubernetes

      # Bearer Token
      def bearer_token
        @bearer_token
      end

      def bearer_token=(value)
        # Validation?
        @bearer_token = value
      end

      def internal_address
        @internal_address
      end

      def internal_address=(value)
        @internal_address
      end

      def init
        @bearer_token = ENV['SERVICEACCOUNT_TOKEN'] || File.read(ENV['SERVICEACCOUNT_TOKEN_FILE'])
        # Set instance variables
        # @internal_address = 'https://kubernetes.default.svc'
        @internal_address = ENV['INTERNAL_ADDRESS'] || ENV['EXTERNAL_ADDRESS']

        # @ca_file = '/var/run/secrets/kubernetes.io/serviceaccount/ca.crt'
        @ca_file = ENV['CA_FILE']
        # Options: https://ruby.github.io/openssl/OpenSSL/SSL/SSLContext.html
        @ctx = OpenSSL::SSL::SSLContext.new
        @ctx.set_params(
          ca_file: @ca_file
        )
      end

      def create_ns( namespace, dry_run: false )
        api = "v1"
        _ns = {
          apiVersion: api,
          metadata: {
            name: namespace
          }
        }

        url= "#{@internal_address}/api/#{api}/namespaces" + (dry_run ? "?dryRun=All" : "")
        # puts "#" * 12
        # puts "URL: " + url
        ###############################
        # wrap in try/catch
        ns_response = Http.auth("Bearer #{bearer_token}").post(url, json: _ns, ssl_context: @ctx)
        ###############################

        # puts ns_response

        ###############################
        # wrap in try/catch
        namespace = JSON.parse(ns_response, object_class: OpenStruct)
        ###############################

        if dry_run
          namespace = _ns.to_o
        end

        # puts "NAMESPACE: #{namespace.metadata.name}"

        return namespace
      end
      def create_role( namespace, dry_run: false )
        api="rbac.authorization.k8s.io/v1"
        _role = {
          apiVersion: api,
          metadata: {
            name: "#{namespace}-full-access",
            namespace: namespace
          },
          rules: [
            {
              apiGroups: ["", "extensions", "apps"],
              resources: ["*"],
              verbs: ["*"]
            },
            {
              apiGroups: ["batch"],
              resources: ["jobs","cronjobs"],
              verbs: ["*"]
            }
          ]
        }

        resource="roles"
        url= "#{@internal_address}/apis/#{api}/namespaces/#{namespace}/#{resource}" + (dry_run ? "?dryRun=All" : "")
        # puts url

        ###############################
        # wrap in try/catch
        role_response = Http.auth("Bearer #{bearer_token}").post(url, json: _role, ssl_context: @ctx)
        ###############################

        # puts role_response

        ###############################
        # wrap in try/catch
        role = JSON.parse(role_response, object_class: OpenStruct)
        ###############################

        # puts "#" * 12
        # puts "ROLE: #{role.metadata.name}"
        return role
      end

      def create_rolebinding( namespace, serviceaccount, role, dry_run: false )
        api="rbac.authorization.k8s.io/v1"
        _rolebinding = {
          apiVersion: api,
          metadata: {
            name: "#{namespace}-#{serviceaccount}",
            namespace: namespace
          },
          subjects: [
            {
              kind: "ServiceAccount",
              name: serviceaccount,
              namespace: namespace
            }
          ],
          roleRef: {
            apiGroup: "rbac.authorization.k8s.io",
            kind: "Role",
            name: role
          }
        }

        resource="rolebindings"
        url= "#{@internal_address}/apis/#{api}/namespaces/#{namespace}/#{resource}" + (dry_run ? "?dryRun=All" : "")
        # puts url

        ###############################
        # wrap in try/catch
        rb_response = Http.auth("Bearer #{bearer_token}").post(url, json: _rolebinding, ssl_context: @ctx)
        ###############################

        # puts rb_response

        ###############################
        # wrap in try/catch
        rolebinding = JSON.parse(rb_response, object_class: OpenStruct)
        ###############################

        # puts "ROLE BINDING: #{rolebinding.metadata.name}"
        # puts "#" * 12
        return rolebinding
      end

      def get_serviceaccount_secret_name( namespace, serviceaccount, dry_run: false )
        # get token name
        # _token_name=`curl -k -s -X GET -H "Authorization: Bearer $_token" https://a680d549-b104-4f93-9e8d-468f8a33fbbc.k8s.ondigitalocean.com/api/v1/namespaces/register-interface-12492752/serviceaccounts/default | jq -r '.secrets[0].name'`
        # puts "#" * 12
        # puts "SERVICEACCOUNT: #{serviceaccount}"
        api="v1"
        resource="serviceaccounts"
        url= "#{@internal_address}/api/#{api}/namespaces/#{namespace}/#{resource}/#{serviceaccount}" + (dry_run ? "?dryRun=All" : "")
        # puts url

        # try/catch
        serviceaccount_response = Http.auth("Bearer #{bearer_token}").get(url, ssl_context: @ctx)
        # puts serviceaccount_response
        serviceaccount = JSON.parse(serviceaccount_response, object_class: OpenStruct)
        # puts serviceaccount
        # puts serviceaccount.secrets[0].name

        serviceaccount.secrets[0].name
      end

      def get_secret( namespace, serviceaccount_secret_name, dry_run: false )
        # get secrets
        # curl -k -s -X GET -H "Authorization: Bearer $_token" "https://a680d549-b104-4f93-9e8d-468f8a33fbbc.k8s.ondigitalocean.com/api/v1/namespaces/register-interface-12492752/secrets/$_token_name" | jq
        # puts "#" * 12
        # puts "SECRET: #{serviceaccount_secret_name}"
        resource="secrets"
        api="v1"
        url= "#{@internal_address}/api/#{api}/namespaces/#{namespace}/#{resource}/#{serviceaccount_secret_name}" + (dry_run ? "?dryRun=All" : "")
        # puts url

        secret_response = Http.auth("Bearer #{bearer_token}").get(url, ssl_context: @ctx)
        secret = JSON.parse(secret_response, object_class: OpenStruct)
        # data contains { ca.crt, namespace, token }
        # ca.crt: curl -k -s -X GET -H "Authorization: Bearer $_token" "https://a680d549-b104-4f93-9e8d-468f8a33fbbc.k8s.ondigitalocean.com/api/v1/namespaces/register-interface-12492752/secrets/$_token_name" | jq -r '.data."ca.crt"' | base64 -d
        # token: curl -k -s -X GET -H "Authorization: Bearer $_token" "https://a680d549-b104-4f93-9e8d-468f8a33fbbc.k8s.ondigitalocean.com/api/v1/namespaces/register-interface-12492752/secrets/$_token_name" | jq -r '.data."token"' | base64 -d

        secret.data
      end

      def create_config( namespace, serviceaccount, dry_run: false )
        serviceaccount_secret_name = get_serviceaccount_secret_name namespace, serviceaccount, dry_run: dry_run

        secret = get_secret namespace, serviceaccount_secret_name, dry_run: dry_run
        username = "lab-environment-#{namespace}-default"
        context = "lab-environment"
        cluster = "lab-environment-#{namespace}"

        {
          apiVersion: 'v1',
          kind: 'Config',
          preferences: {},
          'current-context': context,
          clusters: [
            {
              cluster: {
                'certificate-authority-data': secret[:'ca.crt'],
                server: ENV['EXTERNAL_ADDRESS']
              },
              name: cluster
            }
          ],
          contexts: [
            {
              context: {
                cluster: cluster,
                namespace: namespace,
                user: username
              },
              name: context
            }
          ],
          users: [
            {
              user: {
                token: Base64.decode64(secret[:token])
              },
              name: username
            }
          ]
        }

      end
    end
  end
end
